//
//  ViewController.swift
//  Network_Check
//
//  Created by Sukhjeet Singh on 02/04/22.
//

import UIKit
import CFNetwork
import Network
import CoreTelephony
import SystemConfiguration.CaptiveNetwork
import CoreLocation

class ViewController: UIViewController {
    
    var currentNetworkInfos: Array<NetworkInfo>? {
        get {
            return fetchNetworkInfo()
        }
    }
    
    var locationManager: CLLocationManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        locationManager = CLLocationManager()
        
        locationManager?.requestAlwaysAuthorization()
        
        //Need Location access to get ssid, bssid, interface, success
        print("ssid: ", currentNetworkInfos?.first?.ssid) //Nil
        print("bssid: ", currentNetworkInfos?.first?.bssid) //Nil
        print("interface: ", currentNetworkInfos?.first?.interface) //en0
        print("success: ", currentNetworkInfos?.first?.success) //false
        
    }
    
    func fetchNetworkInfo() -> [NetworkInfo]? {
        if let interfaces: NSArray = CNCopySupportedInterfaces() {
            var networkInfos = [NetworkInfo]()
            for interface in interfaces {
                let interfaceName = interface as! String
                var networkInfo = NetworkInfo(interface: interfaceName,
                                              success: false,
                                              ssid: nil,
                                              bssid: nil)
                if let dict = CNCopyCurrentNetworkInfo(interfaceName as CFString) as NSDictionary? {
                    networkInfo.success = true
                    networkInfo.ssid = dict[kCNNetworkInfoKeySSID as String] as? String
                    networkInfo.bssid = dict[kCNNetworkInfoKeyBSSID as String] as? String
                }
                networkInfos.append(networkInfo)
            }
            return networkInfos
        }
        return nil
    }
    
    
}

struct NetworkInfo {
    var interface: String
    var success: Bool = false
    var ssid: String?
    var bssid: String?
}
